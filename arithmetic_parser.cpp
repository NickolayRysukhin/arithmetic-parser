//-----------------------------------------------------------------------
//  File        : arithmetic_parser.cpp
//  Created     : 26.07.2021
//  Author      : Nickolay Rysukhin <rysukhin@mail.ru>
//  Description : Разбор и вычисление арифметических выражений
//              : в инфиксной и обратной польской нотациях
//-----------------------------------------------------------------------

#include "arithmetic_parser.h"
#include <stdarg.h>

int aparser::StackEntry::operands_count() const
{
    switch(type)
    {
    case ST_VALUE:
        return 0;
    case ST_NEGATE:
    case ST_SIN:
    case ST_COS:
    case ST_SQRT:
    case ST_EXP:
    case ST_LN:
        return 1;
    default:
        return 2;
    }
}

bool aparser::StackEntry::is_operator() const
{
    return type != ST_VALUE;
}


double aparser::StackEntry::calc_unary(double op1) const
{
    switch(type)
    {
        case ST_NEGATE: return -op1;
        case ST_SIN:    return sin(op1);
        case ST_COS:    return cos(op1);
        case ST_SQRT:   return sqrt(op1);
        case ST_EXP:    return exp(op1);
        case ST_LN:     return log(op1);
    }

    return NAN;
}

double aparser::StackEntry::calc_binary(double op1, double op2) const
{
    switch(type)
    {
    case ST_ADD:
        return op1 + op2;
    case ST_SUBTRACT:
        return op1 - op2;
    case ST_MULTIPLY:
        return op1 * op2;
    case ST_DIVIDE:
        return op1 / op2;
    case ST_POWER:
        return powf(op1, op2);
    default:
        return NAN;
    }
}

const char* aparser::StackEntry::operator_name() const
{
    switch(type)
    {
    case ST_ADD:        return "+";
    case ST_SUBTRACT:   return "-";
    case ST_MULTIPLY:   return "*";
    case ST_DIVIDE:     return "/";
    case ST_POWER:      return "^";
    case ST_LPARENT:    return "(";
    case ST_RPARENT:    return ")";
    // Функции
    case ST_SIN:        return "sin";
    case ST_COS:        return "cos";
    case ST_SQRT:       return "sqrt";
    case ST_EXP:        return "exp";
    case ST_LN:         return "ln";
    }
    return "";
}

int aparser::StackEntry::operator_precedence() const
{
    switch(type)
    {
    case ST_ADD:        return 2;
    case ST_SUBTRACT:   return 2;
    case ST_MULTIPLY:   return 3;
    case ST_DIVIDE:     return 3;
    case ST_POWER:      return 4;
    case ST_LPARENT:    return 5;
    case ST_RPARENT:    return 5;
    // Унарные операторы, функции.
    case ST_NEGATE:     return 9;
    case ST_SIN:        return 9;
    case ST_COS:        return 9;
    case ST_SQRT:       return 9;
    case ST_EXP:        return 9;
    case ST_LN:         return 9;
    }
    return 0;
}

bool aparser::StackEntry::left_associative() const
{
    return ! (type == ST_POWER);
}

std::string aparser::StackEntry::to_string() const
{
    if(type == ST_VALUE)
    {
        char buf[64];
        sprintf(buf, "%f", value);
        return buf;
    }
    else
        return operator_name();
}

bool aparser::StackEntry::is_function() const
{
    switch(type)
    {
    case ST_COS:
    case ST_SIN:
    case ST_SQRT:
    case ST_EXP:
    case ST_LN:
        return true;
    }
    return false;
}

aparser::StackEntry aparser::StackEntry::make_operator(const std::string& token, int source_pos)
{
    aparser::StackEntryType st = ST_UNKNOWN;
    if(token == "+")            st = ST_ADD;
    else if(token == "-")       st = ST_SUBTRACT;
    else if(token == "*")       st = ST_MULTIPLY;
    else if(token == "/")       st = ST_DIVIDE;
    else if(token == "^")       st = ST_POWER;
    else if(token == "(")       st = ST_LPARENT;
    else if(token == ")")       st = ST_RPARENT;
    else if(token == "sin")     st = ST_SIN;
    else if(token == "cos")     st = ST_COS;
    else if(token == "sqrt")    st = ST_SQRT;
    else if(token == "exp")     st = ST_EXP;
    else if(token == "ln")      st = ST_LN;
    return aparser::StackEntry(st, source_pos);
}

aparser::Parser::Parser()
{
    for(int i = 0; i < ST_UNKNOWN; ++i)
    {
        aparser::StackEntry s(static_cast<aparser::StackEntryType>(i), -1);

        if(s.is_function())
            m_functionTokens.insert(s.operator_name());
    }
}

void aparser::Parser::set_value(const char* name, double val)
{
    m_valueTokens[name] = val;
}

double aparser::Parser::get_value(const char* name) const
{
    auto itr = m_valueTokens.find(name);
    if(itr == m_valueTokens.end())
        return NAN;

    return itr->second;
}

void aparser::Parser::clear_value(const char* name)
{
    m_valueTokens.erase(name);
}

void aparser::Parser::clear_values()
{
    m_valueTokens.clear();
}

double aparser::Parser::evaluate() const
{
    err_clear();
    std::deque<aparser::StackEntry> iteration_src = m_rpnStack;
    std::deque<aparser::StackEntry> iteration_res;

    int operators_count = 2^16;
    bool parse_error = false;

    while((operators_count > 0) && (parse_error == false))
    {
        operators_count = 0;

        while(!iteration_src.empty())
        {
            const aparser::StackEntry& e = iteration_src.front();

            int Ns = iteration_res.size();

            if(e.is_operator())
            {
                if(Ns < e.operands_count())
                {
                    err_print(e.source_pos, "insufficinet arguments for operator, need %d, has only %d", e.operands_count(), Ns);
                    parse_error = true;
                    break;
                }

                if(e.operands_count() == 1)
                {
                    const aparser::StackEntry& first_operand = iteration_res.back();
                    if(first_operand.is_operator())
                    {
                        iteration_res.push_back( e );          // Операнд ещё не вычислен, кладем текущий операнд в стек и продолжаем
                        ++operators_count;
                    }
                    else
                    {
                        aparser::StackEntry res(ST_VALUE, e.calc_unary(first_operand.value));
                        iteration_res.pop_back();              // Операнд есть, удаляем его из стека и помещаем в стек результат вычисления
                        iteration_res.push_back(res);
                    }
                }
                else if(e.operands_count() == 2)
                {
                    const aparser::StackEntry& first_operand = iteration_res.at(Ns-2);
                    const aparser::StackEntry& second_operand = iteration_res.at(Ns-1);

                    if(first_operand.is_operator() || second_operand.is_operator())
                    {
                        iteration_res.push_back( e );          // Операнд ещё не вычислен, кладем текущий операнд в стек и продолжаем
                        ++operators_count;
                    }
                    else
                    {
                        aparser::StackEntry res(ST_VALUE, e.calc_binary(first_operand.value, second_operand.value));
                        iteration_res.pop_back();
                        iteration_res.pop_back();              // Операнды есть, удаляем их из стека и помещаем в стек результат вычисления
                        iteration_res.push_back(res);
                    }
                }
                else
                {
                    err_print(e.source_pos, "operators with operans count = %d not supported", e.operands_count());
                    parse_error = true;
                    break;
                }
            }
            else
            {
                iteration_res.push_back(e);
            }

            iteration_src.pop_front();
        }

        if(operators_count > 0)
        {
            iteration_src = iteration_res;
            iteration_res.clear();
        }
    }

    if(iteration_res.empty())
    {
        err_print(-1, "empty result");
        return NAN;
    }

    if(iteration_res.size()>1)
        err_print(-1, "WARNING: more than one value at finish, incorrect expression");

    return iteration_res.back().value;
}

bool  aparser::Parser::parse_rpn(const std::string& input)
{
    err_clear();
    m_rpnStack.clear();
    std::deque<aparser::StackEntry>&  retval = m_rpnStack;

    std::string v = input;
    int cur_pos = 0;
    const std::string delimiters = " \t";

    while(cur_pos < v.size())
    {
        int res_begin = input.find_first_not_of(delimiters, cur_pos);               // Пропускаем разделители в начале

        if(res_begin == std::string::npos)
            break;

        int res_end = input.find_first_of(delimiters, res_begin);                   // Ищем следующий разделитель

        auto token = input.substr(res_begin, res_end-res_begin);

        //printf("token (%d, %d)=%s\n", res_begin, res_end, token.c_str());

        retval.push_back(aparser::StackEntry::make_operator(token, res_begin));

        if(retval.back().type == ST_UNKNOWN)
            retval.back() = aparser::StackEntry(parse_value(token.c_str()), res_begin);

        if(res_end == std::string::npos)
            break;

        cur_pos = res_end+1;
    }
    return true;
}

// Shunting-yard algorithm
// https://en.wikipedia.org/wiki/Shunting-yard_algorithm
bool   aparser::Parser::parse_infix(const std::string& input)
{
    err_clear();
    m_rpnStack.clear();
    std::deque<aparser::StackEntry>&  retval = m_rpnStack;

    std::string v = input;
    unsigned int cur_pos = 0;

    std::deque<aparser::StackEntry> output, operator_stack;

    const std::string delimiters = " \t";
    const std::string operators = "()+-*/^";

    while(cur_pos < input.size() && cur_pos != std::string::npos)
    {
        int res_begin = input.find_first_not_of(delimiters, cur_pos);               // Пропускаем пустые разделители в начале

        if(res_begin == std::string::npos)
            break;

        int res_end = input.find_first_of(delimiters+operators, res_begin);         // Ищем следующий разделитель или оператор

        if(res_begin != res_end)
        {
            std::string token = input.substr(res_begin, res_end - res_begin);

            if(m_functionTokens.find(token) != m_functionTokens.end())
            {
                //printf( "function(%d, %d)=%s\n", res_begin, res_end, token.c_str());
                operator_stack.push_front( aparser::StackEntry::make_operator(token, res_begin) );
            }
            else
            {
                //printf( "token(%d, %d)=%s\n", res_begin, res_end, token.c_str());
                double val = parse_value(token.c_str());
                if(std::isnan(val))
                {
                    err_print(res_begin, "unkonwn token: %s", token.c_str());
                    break;
                }
                output.push_back( aparser::StackEntry( val, res_begin) );
            }
        }

        if(res_end != std::string::npos &&                              // Тут может быть оператор
              operators.find(input[res_end]) != std::string::npos  )    // Точно оператор, записываем его, продолжаем с начала цикла
        {
            // input[res_end] -> operators stack or so on...
            std::string cur_op_str;
            cur_op_str.append(1, input[res_end]);

            //printf( "operator(%d, %d)=%s\n", res_begin, res_end, cur_op_str.c_str());
            auto cur_op = aparser::StackEntry::make_operator(cur_op_str, res_begin);

            if(cur_op.type == ST_UNKNOWN)
            {
                err_print(cur_op.source_pos, "unkonwn operator: %s", cur_op_str.c_str());
                break;
            }

            if(cur_op.type == ST_LPARENT)
            {
                operator_stack.push_front(cur_op);
            }
            else if(cur_op.type == ST_RPARENT)
            {
                while(!operator_stack.empty() &&
                      operator_stack.front().type != ST_LPARENT)
                {
                    output.push_back( operator_stack.front() );
                    operator_stack.pop_front();
                }

                if(operator_stack.empty() || operator_stack.front().type != ST_LPARENT)
                {
                    err_print(cur_op.source_pos, "parenthesis mismatch, ')' has no corresponding '('");
                    break;
                }
                operator_stack.pop_front();
            }
            else
            {
                while(!operator_stack.empty() &&
                     operator_stack.front().type != ST_LPARENT &&
                      (operator_stack.front().operator_precedence() > cur_op.operator_precedence() ||
                       (operator_stack.front().operator_precedence() == cur_op.operator_precedence() && cur_op.left_associative() )))
                {
                    output.push_back( operator_stack.front() );
                    operator_stack.pop_front();
                }
                operator_stack.push_front(cur_op);
            }
        }

        if(res_end != std::string::npos)
            res_end++;

        cur_pos = res_end;
    }

//    printf("OUTPUT=");
//    for(const auto& se : output)
//        printf("%s ", se.to_string().c_str());
//    printf("\n");

//    printf("OPERATOR_STACK=");
//    for(const auto& se : operator_stack)
//        printf("%s ", se.to_string().c_str());
//    printf("\n");

    while(!operator_stack.empty())
    {
        if(operator_stack.front().type == ST_LPARENT)
        {
            err_print(operator_stack.front().source_pos, "parenthesis mismatch, '(' has no corresponding ')'");
            break;
        }
        output.push_back( operator_stack.front() );
        operator_stack.pop_front();
    }

    m_rpnStack = output;

//    printf("RPN=");
//    for(const auto& se : m_rpnStack)
//        printf("%s ", se.to_string().c_str());
//    printf("\n");

    return true;
}

const char* aparser::Parser::error_str() const
{
    return m_errorDescription.c_str();
}

int         aparser::Parser::error_pos() const
{
    return m_errorPos;
}

double aparser::Parser::parse_value( const std::string& value) const
{
    const auto& itr = m_valueTokens.find(value);

    if(itr == m_valueTokens.end())
    {
        float v = NAN;
        double retval = NAN;
        if(sscanf(value.c_str(), "%f", &v) == 1)
            retval = v;

        return retval;
    }
    return itr->second;
}

void aparser::Parser::err_clear() const
{
    m_errorDescription.clear();
    m_errorPos = -1;
}

void aparser::Parser::err_print(int pos, const char* _format, ...) const
{
    char buffer[256];
    m_errorPos = pos;
    va_list v;
    va_start(v, _format);
    vsprintf(buffer, _format, v);
    m_errorDescription = buffer;
}
