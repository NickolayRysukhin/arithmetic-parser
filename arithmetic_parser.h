//-----------------------------------------------------------------------
//  File        : arithmetic_parser.h
//  Created     : 26.07.2021
//  Author      : Nickolay Rysukhin <rysukhin@mail.ru>
//  Description : Разбор и вычисление арифметических выражений
//              : в инфиксной и обратной польской нотациях
//-----------------------------------------------------------------------

#ifndef __ARITHMETIC_PARSER__H__
#define __ARITHMETIC_PARSER__H__

#include <deque>
#include <cmath>
#include <string>
#include <set>
#include <map>

namespace aparser {
    enum    StackEntryType
    {
        ST_VALUE,
        ST_ADD,         // +
        ST_SUBTRACT,    // -
        ST_MULTIPLY,    // *
        ST_DIVIDE,      // /
        ST_NEGATE,      // ???
        ST_POWER,       // ^
        ST_LPARENT,     // (
        ST_RPARENT,     // )

        ST_SIN,         // sin
        ST_COS,         // cos
        ST_SQRT,        // sqrt
        ST_EXP,         // exp
        ST_LN,          // ln

        ST_UNKNOWN      // ошибка разбора
    };

    struct StackEntry
    {
        StackEntry() : type(ST_VALUE) { value = NAN; }
        StackEntry(const StackEntry& _o) : type(_o.type), value(_o.value), source_pos(_o.source_pos) {}
        StackEntry(StackEntryType _t, int _sp) : type(_t), source_pos(_sp) { value = NAN; }
        StackEntry(double _v, int _sp) : type(ST_VALUE), value(_v), source_pos(_sp) {}
        StackEntry(StackEntryType _t, double _v) : type(_t), value(_v) {}

        StackEntryType type;
        double value;           // Значение (для операторов = NAN)
        int source_pos = -1;    // Положение (начало) оператора или значения в исходной строке (для сообщений об ошибках)

        int operands_count() const;

        bool is_operator() const;

        double calc_unary(double op1) const;

        double calc_binary(double op1, double op2) const;

        const char* operator_name() const;

        int operator_precedence() const;
        bool left_associative() const;

        std::string to_string() const;

        bool is_function() const;

        static StackEntry make_operator(const std::string& token, int source_pos = -1);
    };

    class Parser
    {
        std::set< std::string >             m_functionTokens;
        std::map< std::string, double>      m_valueTokens;
        std::deque<StackEntry>              m_rpnStack;

        mutable std::string                 m_errorDescription;
        mutable int                         m_errorPos = -1;

    public:
        Parser();

        // Установка переменной (или константы) по имени
        void        set_value(const char* name, double val);
        // Получение переменной (или константы) по имени
        double      get_value(const char* name) const;
        // Сброс переменной (или константы)
        void        clear_value(const char* name);
        // Сброс всех переменных
        void        clear_values();

        // Расчет выражения
        double      evaluate() const;

        // Разбор строки в обратной польской нотации
        bool        parse_rpn(const std::string& input);
        // Разбор строки в инфиксной нотации
        bool        parse_infix(const std::string& input);

        // Получение строкового описания ошибки
        const char* error_str() const;
        // Получение номера символа в исходной строке, на котором возникла ошибка (-1 если положение определить не удалось)
        int         error_pos() const;
        // Получение признака наличия ошибки (true-ошибка есть)
        bool        error() const { return !m_errorDescription.empty(); }

    private:
        double parse_value( const std::string& value) const;
        void err_clear() const;
        void err_print(int pos, const char* _format, ...) const;
    };
}

#endif
