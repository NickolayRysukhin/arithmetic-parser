
#include <iostream>
#include <string.h>
#include "arithmetic_parser.h"

void print_err_descr(const aparser::Parser& p, const char* input_expression, const char* err_common_descr)
{
    if(p.error())
    {
        printf("%s: %s", err_common_descr, p.error_str());
        int pos = p.error_pos();
        if(pos >= 0)
        {
            printf(" at:\n%s\n", input_expression);
            printf ("%*s\n", pos+1, "^");
        }
        else
            printf("\n", input_expression);
    }
}

int main(int argc, char *argv[])
{
    //    std::deque<StackEntry> expr;
    //    expr.push_back(4.5);
    //    expr.push_back(3.7);
    //    expr.push_back(ST_ADD);
    //    double r1 = evaluate_rpn(expr);
    //    printf("r1 = %f\n", r1);


    std::istream& input_stream = std::cin;
    std::string input_expression;

    aparser::Parser p;
    p.set_value("PI", 3.14159265359);
    p.set_value("E", 2.71828182846);

    if(argc > 1 && strcmp(argv[1], "rpn")==0)
    {
        while (true)
        {
            std::cout << "> ";
            if (!std::getline(input_stream, input_expression))
                break;

            p.parse_rpn(input_expression);
            print_err_descr(p, input_expression.c_str(), "RPN parse error");
            double r = p.evaluate();
            print_err_descr(p, input_expression.c_str(), "Evaluate error");

            printf("RPN Result = %f\n", r);

        }
    }
    else
    {
        while (true)
        {
            std::cout << "> ";
            if (!std::getline(input_stream, input_expression))
                break;

            p.parse_infix(input_expression);
            print_err_descr(p, input_expression.c_str(), "Infix parse error");

            double r = p.evaluate();
            print_err_descr(p, input_expression.c_str(), "Evaluate error");

            printf("Result = %f\n", r);
        }
    }


    return EXIT_SUCCESS;
}
